var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat');

    gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});
gulp.task('default', function() {

}) ;

gulp.task('sass', function () {
    gulp.src('./sass/**/*.scss')
        .pipe(sass({outputStyle:"compressed"}).on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('js', function() {
    gulp.src(['./scripts/*.js'])
        .pipe(concat('script.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('js/'))
});

gulp.task('sass:watch', function () {
    gulp.watch('*.scss', ['sass']);
});

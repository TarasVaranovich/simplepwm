function priod_calculation(Ud,Umax,prec_a, dataArr) {
  var U1 = a = t1 = t2 = t3 = Ua = Ub = Uc = ka = kb = kc = 0.111111;
  var a2 = 0;
  var alfa1 = alfa2 = 0.111111;
  //dataArr.length = 0;
  for (var a = 0; a <= 360; a += 10) {

      U1 = Umax * Math.sin(a * Math.PI / 180);
      alfa1 = Math.PI * (a2 + 60 - a) / 180.0;
      alfa2 = Math.PI * (a - a2) / 180.0;
      t2 = Math.sqrt(3) * (U1 / Ud) * Math.sin(alfa1);
      t3 = Math.sqrt(3) * (U1 / Ud) * Math.sin(alfa2);
      if ((a == 30) || (a == 90) || (a == 150) || (a == 210) || (a == 270)) {
          t1 = 0;
      } else {
          t1 = 1 - (t2 + t3);
      }
      switch (a2) {

          case 0:
              ka = 0.666667;
              kb = -0.333333;
              kc = -0.333333;

              break;
          case 60:
              ka = 0.333333;
              kb = 0.333333;
              kc = -0.666667;

              break;
          case 120:
              ka = -0.333333;
              kb = 0.666667;
              kc = -0.333333;

              break;
          case 180:
              ka = -0.666667;
              kb = 0.333333;
              kc = 0.333333;

              break;
          case 240:
              ka = -0.333333;
              kb = -0.333333;
              kc = 0.666667;

              break;
          case 300:
              ka = 0.333333;
              kb = -0.666667;
              kc = 0.333333;

              break;
          default:
              ka = 0;
              kb = 0;
              kc = 0;

              break;
      }
      Ua = ka * Ud;
      Ub = kb * Ud;
      Uc = kc * Ud;

      var dataTupple = {id:a, U1:U1, t1:t1, t2:t2, t3:t3, Ua:Ua, Ub:Ub, Uc:Uc};
      dataArr.push(dataTupple);
      data_acceptance (a, U1, t1, t2, t3, Ua, Ub, Uc);
      if (a % 60 == 0 && a != 0) {
          a2 += 60;
      }
  }
}
